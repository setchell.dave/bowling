# Requirements and Input Spec
Utilizing a language of your choice, in a style of your choice, implement the 
rules of bowling such that if provided a list of rolls and pins knocked down 
each roll, the program provides the final score, the running score for each 
frame, and a record of whether the bowler had a strike, spare, or open frame 
for each frame.  Then add the ability for players to configure custom/house 
rules of their own. 

### input:
list of list of ints. list index represents the roll. list value is the pins ala

```
    1
   2 3
  4 5 6
 7 8 9 10
```

Each pin knocked down is represented in the list by number.
so a gutter ball's list value is the empty list
and a seven/ten split could be the list: [1,2,3,4,5,7,10]

Spares and strikes are derived; meaning no special set values.
However a foul will be represented by a list of only "0"

# Use

After building, the command "bowling" will be created.

It is very simple:
	`bowling file_names...`

Each file is a json list of rolls; which are json lists of ints. Each file passed
as an arguemnt represents a player's game. 
The output will show the player number, the file from which the player roll 
data was imported, and then the computed Frame data.

There are some sample json files in the "sample_input" directory.

# Issues
Happy to talk about the decisions here. Primary reason to leave these issues in place
is to keep the worked time near what was asked for in the email. Current work time is 
at just over 4 hours.

- Program relies on input to be good and sane. Specifically "impossible" rolls could occur
w/o error. For example, a first roll of the frame that hit pins 1 and 2 then a second roll of that 
frame could have 1 and 2 in it's list of pins. Idea is that the roll data would be validated
further outside this part of the program.

- Config is simply a few consts right now and with limited rule change domain. You can change
the number of rolls ahead that are included in Strike or Spare frames. Additionally, you can
change the number of frames that define a "game". Wasn't sure how much flexibility was desired
here. Also the tests do not take this into account.

- The Frame structure has a field, "FinalFrame", that only really matters to the final frame
for storing the possibilites of more strikes. Additionally it shows the roll numbers here
regardless of their values.


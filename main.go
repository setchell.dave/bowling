package main

import (
	"bowling/score"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	var rs []score.Roll
	for pnum, fname := range os.Args[1:] {
		rolls, err := ioutil.ReadFile(fname)
		if err != nil {
			fmt.Println(err)
			return
		}
		err = json.Unmarshal(rolls, &rs)
		if err != nil {
			fmt.Println(err)
			return
		}
		c, err := score.CalcFrames(rs)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("Player %d from input file %s\n", pnum+1, fname)
		for i, cc := range c {
			fmt.Printf("frame: %d, %+v\n", i+1, *cc)
		}
	}
}

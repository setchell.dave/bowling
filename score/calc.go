package score

const (
	MAX_FRAME    int = 10
	STRIKE_AHEAD int = 2
	SPARE_AHEAD  int = 1
)

type frameStatus int

const (
	Strike frameStatus = iota
	Spare
	Standard
	Foul
	Open
	FS_END
)

func (f frameStatus) String() string {
	fs := [...]string{"strike", "spare", "standard", "foul", "open"}
	if f < Strike || f >= FS_END {
		return ""
	}
	return fs[f]
}

type splitStatus int

const (
	Split splitStatus = iota
	Wide
	None
	SS_END
)

func (s splitStatus) String() string {
	ss := [...]string{"split", "wide", "none"}
	if s < Split || s >= SS_END {
		return ""
	}
	return ss[s]
}

type Roll []int
type Frame struct {
	FrameScore   int
	RunningScore int
	Split        splitStatus
	Status       frameStatus
	FinalFrame   []int
}

func in(m map[int]struct{}, v int) bool {
	_, ok := m[v]
	return ok
}

func isSplit(r Roll) splitStatus {
	var set = make(map[int]struct{})
	var split bool
	for _, pin := range r {
		set[pin] = struct{}{}
	}
	if (in(set, 4) && in(set, 6) && !in(set, 5)) ||
		(in(set, 7) && in(set, 10) && (!in(set, 8) || !in(set, 9))) {
		split = true
	}
	if split && in(set, 1) {
		return Split
	} else if split && !in(set, 1) {
		return Wide
	} else {
		return None
	}
}

func lookahead(rolls []Roll, num int) int {
	sum := 0
	for i, r := range rolls {
		if i >= num {
			break
		}
		if len(r) > 0 && r[0] == 0 {
			continue
		}
		sum += len(r)
	}
	return sum
}

func CalcFrames(rolls []Roll) ([]*Frame, error) {
	var result []*Frame
	newFrame := true
	var curFrame *Frame
	rScore := 0
	for i, r := range rolls {
		if newFrame {
			curFrame = &Frame{}
			result = append(result, curFrame)
			// splitStatus only relevant on new frames
			curFrame.Split = isSplit(r)
			curFrame.FrameScore = len(r)
			switch curFrame.FrameScore {
			case 10:
				curFrame.Status = Strike
				newFrame = len(result) != MAX_FRAME
				curFrame.FrameScore += lookahead(rolls[i+1:], STRIKE_AHEAD)
				rScore += curFrame.FrameScore
			case 1:
				if r[0] == 0 {
					curFrame.FrameScore = 0 // correction from length calc
					curFrame.Status = Foul
				}
				newFrame = false
			default:
				curFrame.Status = Standard
				newFrame = false
			}
			if len(result) == MAX_FRAME {
				for ii, rr := range rolls[i:] {
					if ii > STRIKE_AHEAD {
						break
					}
					if len(rr) > 0 && rr[0] == 0 {
						continue
					}
					curFrame.FinalFrame = append(curFrame.FinalFrame, len(rr))
				}
				if curFrame.Status == Strike {
					curFrame.RunningScore = rScore
					break
				}
			}
		} else {
			curFrame = result[len(result)-1]
			curFrame.FrameScore += len(r)
			if len(r) == 1 && r[0] == 0 {
				curFrame.FrameScore -= 1
				curFrame.Status = Foul
				newFrame = len(result) != MAX_FRAME
			} else if curFrame.FrameScore == 0 {
				curFrame.Status = Open
				newFrame = len(result) != MAX_FRAME
			} else if curFrame.FrameScore == 10 {
				curFrame.Status = Spare
				newFrame = len(result) != MAX_FRAME
				curFrame.FrameScore += lookahead(rolls[i+1:], SPARE_AHEAD)
			} else {
				curFrame.Status = Standard
				newFrame = len(result) != MAX_FRAME
			}
			rScore += curFrame.FrameScore
			if len(result) == MAX_FRAME {
				curFrame.RunningScore = rScore
				break
			}
		}
		curFrame.RunningScore = rScore
	}
	return result, nil
}

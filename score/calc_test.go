package score

import (
	"testing"
)

var full []Roll = []Roll{
	{1, 2, 3},
	{0},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 6, 7, 8, 9, 10},
	{4, 5},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{2, 3, 4, 5, 6, 7, 8, 9, 10},
	{},
	{3, 4, 8},
	{5, 1},
	{2, 3, 4, 5, 6, 7, 8, 9, 10},
	{},
	{2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1},
	{7},
	{2, 3},
	{4, 3},
	{5},
}

var partial = full[:len(full)-4]
var perfect []Roll = []Roll{
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
}

var finalSpare []Roll = []Roll{
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9},
	{10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
}

var showSplit []Roll = []Roll{
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 6, 7, 8, 9, 10},
	{},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{2, 3, 4, 5, 6, 7, 9, 10},
	{1},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9},
	{10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
}

var showOpen []Roll = []Roll{
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 6, 7, 8, 9, 10},
	{},
	{},
	{},
	{2, 3, 4, 5, 6, 7, 9, 10},
	{1},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9},
	{10},
	{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
}

func showRoll(t *testing.T, name string, fs []*Frame) {
	t.Logf("Showing frames for %s\n", name)
	for i, f := range fs {
		t.Logf("Frame %d: %+v\n", i+1, *f)
	}
}

func TestCalcFrames(t *testing.T) {
	fs, err := CalcFrames(full)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	if fs[len(fs)-1].RunningScore != 102 {
		t.Errorf("full failed with score: %d, instead of 102\n", fs[len(fs)-1].RunningScore)
	}
	showRoll(t, "full", fs)

	fs, err = CalcFrames(partial)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	// foul test here is a bit opaque
	if fs[len(fs)-1].RunningScore != 95 || len(fs) != 8 || fs[0].Status != Foul {
		t.Errorf("partial failed with score: %d, instead of 95 and length %d instead of 8\n", fs[len(fs)-1].RunningScore, len(fs))
	}
	showRoll(t, "partial", fs)

	fs, err = CalcFrames(perfect)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	if fs[len(fs)-1].RunningScore != 300 {
		t.Errorf("perfect failed with score: %d, instead of 300\n", fs[len(fs)-1].RunningScore)
	}
	showRoll(t, "perfect", fs)

	fs, err = CalcFrames(finalSpare)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	if fs[len(fs)-1].RunningScore != 279 {
		t.Errorf("final spare failed with score: %d, instead of 279\n", fs[len(fs)-1].RunningScore)
	}
	showRoll(t, "final spare", fs)

	fs, err = CalcFrames(showSplit)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	if fs[1].Split != Split {
		t.Errorf("split not identified\n")
	}
	if fs[4].Split != Wide {
		t.Errorf("wide not identified\n")
	}
	showRoll(t, "split varieties", fs)

	fs, err = CalcFrames(showOpen)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	if fs[2].Status != Open {
		t.Errorf("open not identified\n")
	}
	showRoll(t, "open frame", fs)
}
